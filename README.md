# C3PO-artifact

Artifact used for the evaluation of the C3PO-2020 paper on compiler analysis for
OmpSs.

The archive C3PO-artifact.tar contains:
*  The software developed and used for the evaluation, including:
    *  Clang/LLVM compilation tool-chain
    *  Nanos6 runtime
*  The benchmarks used for the evaluation